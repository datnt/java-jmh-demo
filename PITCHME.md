# JMH?

- Getting Started
- JMH Benchmark Modes
- Benchmark Time Units


---
# Getting Started

```bash
 mvn archetype:generate \
          -DinteractiveMode=false \
          -DarchetypeGroupId=org.openjdk.jmh \
          -DarchetypeArtifactId=jmh-java-benchmark-archetype \
          -DgroupId=pro.datnt \
          -DartifactId=java-jmh-demo \
          -Dversion=1.0.0
```
---
```java
public class MyBenchmark {

    @Benchmark
    public void testMethod() {
        // This is a demo/sample template for building your JMH benchmarks. Edit as needed.
        // Put your benchmark code here.

        int a = 1;
        int b = 2;
        int sum = a + b;
    }

}
```
---
```bash
java -jar target/benchmarks.jar
```

---
# JMH Benchmark Modes

- Throughput
- Average Time
- Sample Time
- Single Shot Time
- All

---
```java
public class MyBenchmark {

    @Benchmark @BenchmarkMode(Mode.Throughput)
    public void testMethod() {
        // This is a demo/sample template for building your JMH benchmarks. Edit as needed.
        // Put your benchmark code here.

        int a = 1;
        int b = 2;
        int sum = a + b;
    }

}
```
---
# Benchmark Time Units

- NANOSECONDS
- MICROSECONDS
- MILLISECONDS
- SECONDS
- MINUTES
- HOURS
- DAYS
---

```java
public class MyBenchmark {

    @Benchmark @BenchmarkMode(Mode.Throughput) @OutputTimeUnit(TimeUnit.MINUTES)
    public void testMethod() {
        // This is a demo/sample template for building your JMH benchmarks. Edit as needed.
        // Put your benchmark code here.

        int a = 1;
        int b = 2;
        int sum = a + b;
    }

}
```
---
# Benchmark State

- State Scope
- State Class Requirements
- State Object @Setup and @TearDown

---
## State Scope

- Thread
- Group
- Benchmark

---
## State Class Requirements

- The class must be declared public
- If the class is a nested class, it must be declared static
- The class must have a public no-arg constructor

---
## State Object @Setup and @TearDown

- Level.Trial
- Level.Iteration
- Level.Invocation

---
```java
public class MyBenchmark {

    @State(Scope.Thread)
    public static class MyState {

        @Setup(Level.Trial)
        public void doSetup() {
            sum = 0;
            System.out.println("Do Setup");
        }

        @TearDown(Level.Trial)
        public void doTearDown() {
            System.out.println("Do TearDown");
        }

        public int a = 1;
        public int b = 2;
        public int sum ;
    }

    @Benchmark @BenchmarkMode(Mode.Throughput) @OutputTimeUnit(TimeUnit.MINUTES)
    public void testMethod(MyState state) {
        state.sum = state.a + state.b;
    }
}
```
---
# Tips

- Loop Optimizations
- Dead Code Elimination
- Constant Folding

---

# Reference
- [Code Tools: jmh](https://openjdk.java.net/projects/code-tools/jmh/)
- [JMH - Java Microbenchmark Harness](http://tutorials.jenkov.com/java-performance/jmh.html#why-are-java-microbenchmarks-hard)
- [Microbenchmarking with Java](https://www.baeldung.com/java-microbenchmark-harness)
